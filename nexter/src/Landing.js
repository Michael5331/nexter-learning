export default function Landing() {
    return (
        <div className="container">
            <p>This is not a real application.</p>
            <p>But thanks for clicking the link
                to test it.
            </p>
        </div>
    )
}